import { run } from './instances/server';
import { synchronizatorCron } from './instances/synchronizatorCron';

async function start() {
    try {
        run();

        const cron = await synchronizatorCron();

        cron.run();

        console.log('application successfully started');
    } catch(e) {
        console.error('error while staring the application');
        console.error(e);
    }

}

start();
