import { MongoClient, Db } from 'mongodb';

export class Mongo {
    config: any
    connection: Db

    constructor(config) {
        this.config = config
    }

    async getConnection(): Promise<Db> {
        if (!this.connection) {
            this.connection = await MongoClient.connect(this.config.uri)
        }

        return this.connection
    }
}
