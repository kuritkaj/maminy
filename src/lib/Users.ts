import { Db, Collection } from 'mongodb';

export interface User {
    wp_id: string
    full_name: string,
    meta: any
}

export class Users {
    collection: Collection;

    constructor(connection: Db) {
        this.collection = connection.collection("users");
    }

    find(query: any) {
        return this.collection
            .find(query.query, query.projection, query.skip, query.limit);
    }

    updateOrInsertOne(user: User) {
        return this.collection
            .updateOne({
                wp_id: user.wp_id
            }, {
                $set: user
            }, {
                upsert: true
            });
    }
}
