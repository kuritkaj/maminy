'use strict';
import {CronJob} from "cron";
import {Synchronizator} from "./Synchronizator";

export class SynchronizatorCron {
    job: CronJob;

    constructor(protected cronPattern: string, protected synchronizator: Synchronizator) {
    }

    run() {
        this.job = new CronJob({
            cronTime: this.cronPattern,
            onTick: () => {
                console.log('synchronizing users with local mongo', new Date());

                this.synchronizator
                    .synchronize()
                    .then(() => {
                        console.log('success');
                    })
                    .catch(e => {
                        console.error(e);
                    });
            },
            runOnInit: true
        });

        this.job.start();
    }
}
