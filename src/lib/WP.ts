'use strict';
const WPAPI = require('wpapi');

export interface WPUser {
    id: string
    name: string
    meta: any
}

interface Requestor {
    (page: number): Promise<any>
}

export class Paginator<T> {
    actualPage = 0;
    response: T[] | any = [];

    constructor(protected requestor: Requestor) {}

    hasNextPage() {
        if (!this.actualPage) {
            return true;
        }

        const paging = this.response._paging;

        return paging && paging.next;
    }

    async loadNextPage() {
        this.response = await this.requestor(++this.actualPage);
    }

    getItems(): T[] {
        return this.response;
    }
}

export class WP {
    api: any;

    constructor(config: any) {
        this.api = new WPAPI(config);
    }

    getUsersPaginator(): Paginator<WPUser> {
        return new Paginator<WPUser>(page => {
            return this.api.users().perPage(10).page(page);
        });
    }
}
