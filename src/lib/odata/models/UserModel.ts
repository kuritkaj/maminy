import { Edm } from "odata-v4-server";

class JoblineCapabilities {
    @Edm.Boolean
    cs_employer: boolean;
}

class MetaData {
    @Edm.String
    email: string;

    @Edm.String
    description: string;

    @Edm.Int32
    current_salary: number;

    @Edm.Int32
    expected_salary: number;

    @Edm.String
    education_level: string;

    @Edm.String
    type: string;

    @Edm.String
    languages: string;

    /*
    JOBLINE
     */
    @Edm.ComplexType(JoblineCapabilities)
    jobline_capabilities: JoblineCapabilities;

    @Edm.Int32
    jobline_user_level: number;

    /*
    JOBHUNT
     */
    @Edm.String
    jobhunt_user_level: string;

    // @Edm.String
    // jobhunt_capabilities: any;

    /*
     CS_
      */
    @Edm.String
    cs_linkedin: string;

    @Edm.String
    cs_google_plus: string;

    @Edm.String
    cs_phone_number: string;

    @Edm.String
    cs_post_loc_country: string;

    @Edm.String
    cs_post_loc_city: string;

    @Edm.String
    cs_job_title: string;

    @Edm.String
    cs_users_status: string;

    @Edm.Int32
    cs_minimum_salary: number;

    @Edm.String
    cs_facebook: string;

    @Edm.String
    cs_twitter: string;

    @Edm.String
    cs_post_comp_address: string;

    @Edm.String
    cs_post_loc_address: string;

    @Edm.String
    cs_post_loc_latitude: string;

    @Edm.String
    cs_post_loc_longitude: string;

    @Edm.String
    cs_add_new_loc: string;

    @Edm.String
    cs_award_name_pop: string;

    @Edm.String
    cs_award_year_pop: string;

    @Edm.String
    cs_award_desc_pop: string;

    @Edm.String
    cs_specialisms: string;

    @Edm.String
    cs_award_name_array: string;

    @Edm.String
    cs_edu_title_array: string;

    @Edm.String
    cs_exp_title_array: string;

    @Edm.String
    cs_skill_title_array: string;

    @Edm.String
    cs_exp_company_array: string;

    @Edm.String
    cs_candidate_cv: string;

    @Edm.String
    cs_cover_letter: string;

    /*
       renamed props
     */
    @Edm.String
    has_driving_licence: string;

    @Edm.String
    i_can_start_working_on: string;

    @Edm.String
    form_of_employment: string;

    @Edm.String
    preferred_place_of_work: string;

    @Edm.String
    preferred_working_hours: string;

    @Edm.String
    management_experience: string;

    @Edm.String
    skype: string;
}

export class UserModel {
    @Edm.Int32
    wp_id: string;

    @Edm.String
    full_name: string;

    @Edm.ComplexType(MetaData)
    meta: MetaData;
}
