import { ODataController, odata, ODataQuery } from "odata-v4-server";
import { createQuery } from "odata-v4-mongodb";
import { Writable } from "stream";
import { UserModel } from '../models/UserModel';
import { users } from '../../../instances/users';

@odata.type(UserModel)
export class UsersController extends ODataController {
    @odata.GET
    async find( @odata.filter filter: ODataQuery, @odata.stream stream: Writable) {
        return (await users())
            .find(createQuery(filter))
            .stream()
            .pipe(stream);
    }
}