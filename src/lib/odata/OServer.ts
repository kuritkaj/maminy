import { ODataServer, odata } from "odata-v4-server";
import { UsersController } from './controllers/UsersController';

@odata.cors
@odata.namespace("Maminy")
@odata.container("MaminyContext")
@odata.controller(UsersController, "Users")
export class OServer extends ODataServer {

}
