import {Dictionary, mapValues, isArray} from 'lodash';
import {WP, WPUser} from './WP';
import {User, Users} from './Users';

const JOIN_SEPARATOR = ',';

const PROPERTIES_MAP = [
    {
        name: "1",
        renameTo: "has_driving_licence"
    },
    {
        name: "2",
        renameTo: "i_can_start_working_on"
    },
    {
        name: "3",
        renameTo: "form_of_employment"
    },
    {
        name: "4",
        renameTo: "preferred_place_of_work"
    },
    {
        name: "5",
        renameTo: "preferred_working_hours"
    },
    {
        name: "current_salary",
        renameTo: "management_experience"
    },
    {
        name: "cs_minimum_salary",
        renameTo: "skype"
    },
    {
        name: "show_experience",
        renameTo: "languages"
    }
];

export class Synchronizator {
    constructor(protected wp: WP, protected users: Users) {}

    async synchronize() {
        const paginator = this.wp.getUsersPaginator();

        while(paginator.hasNextPage()) {
            try {
                await paginator.loadNextPage();

                const wpUsers = paginator.getItems();

                await this.doYourWork(wpUsers);
            } catch (e) {
                console.error('problem while synchronizing users', e);
            }
        }
    }

    async doYourWork(users: WPUser[]) {
        const transformed = users.map(this.transformUser.bind(this));

        await transformed.map(this.updateUser.bind(this));
    }

    transformUser(user: WPUser): User {
        return {
            wp_id: user.id,
            full_name: user.name,
            meta: this.transformMeta(user.meta)
        };
    }

    /**
     * Sharepoint does not support array data.
     * So arrays will be joined with some separator
     *
     * @param {Dictionary<any>} meta
     * @returns {Dictionary<any>}
     */
    transformMeta(meta: Dictionary<any>) {
        this.renameSomeProperties(meta);

        return this.serializeArrayToString(meta);
    }

    renameSomeProperties(meta: Dictionary<any>) {
        PROPERTIES_MAP.forEach(prop => {
            meta[prop.renameTo] = meta[prop.name] || "";
        });
    }

    serializeArrayToString(meta: Dictionary<any>) {
        return mapValues(meta, value => {
            if (!isArray(value)) {
                return value;
            }

            return value.join(JOIN_SEPARATOR);
        });
    }

    updateUser(user: User) {
        return this.users.updateOrInsertOne(user);
    }
}
