const config = require('config');
import { Db } from 'mongodb';
import { Mongo } from '../lib/Mongo';

const mongo = new Mongo(config.mongo);

export async function mongodb(): Promise<Db> {
    return await mongo.getConnection();
}