import { wp } from './wp';
import { users } from './users';
import {Synchronizator} from "../lib/Synchronizator";

let synchronizatorService: Synchronizator = null;

export async function synchronizator(): Promise<Synchronizator> {
    if (!synchronizatorService) {
        synchronizatorService = new Synchronizator(wp(), await users());
    }

    return synchronizatorService;
}
