const config = require('config');
import {WP} from "../lib/WP";

let WPService: WP = null;

export function wp(): WP {
    if (!WPService) {
        WPService = new WP(config.wp.api);
    }

    return WPService;
}
