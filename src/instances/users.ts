import { Users } from '../lib/Users';
import { mongodb } from './mongo';

let usersService: Users = null

export async function users(): Promise<Users> {
    if (!usersService) {
        usersService = new Users(await mongodb())
    }

    return usersService
}