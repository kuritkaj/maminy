const config = require('config');
import {OServer} from "../lib/odata/OServer";
const express = require("express");

export function run() {
    const app = express();

    app.use('/odata.xml.svc', (req, res) => {
        res.set('Access-Control-Allow-Origin', '*');
        res.set('Content-Type', 'text/xml');

        res.send(`
<service xmlns="http://www.w3.org/2007/app" 
         xmlns:atom="http://www.w3.org/2005/Atom"
         xmlns:m="http://docs.oasis-open.org/odata/ns/metadata" 
         xml:base="http://52.174.251.161:3000/odata.svc/" 
         m:context="http://52.174.251.161:3000/odata.svc/$metadata">
    <workspace>
        <atom:title type="text">Maminy</atom:title>
        <collection href="Users">
            <atom:title type="text">Users</atom:title>
        </collection>
    </workspace>
</service>
`);
    });

    app.use(config.odata.mountPath, OServer.create());

    app.listen(config.odata.port, () => {
        console.log('server stared on port ', config.odata.port);
    });
}
