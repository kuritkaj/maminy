const config = require('config');
import {SynchronizatorCron} from "../lib/SynchronizatorCron";
import {synchronizator} from "./synchronizator";

let cronService: SynchronizatorCron = null;

export async function synchronizatorCron(): Promise<SynchronizatorCron> {
    if (!cronService) {
        cronService = new SynchronizatorCron(config.synchronizator.cronPattern, await synchronizator());
    }

    return cronService;
}

