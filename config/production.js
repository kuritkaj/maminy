'use strict';
module.exports = {
    wp: {
        api: {
            username: process.env.WP_USER,
            password: process.env.WP_PASS
        }
    },
    mongo: {
        uri: process.env.MONGO_URI
    },
    odata: {
        mountPath: '/odata.svc',
        port: Number(process.env.PORT)
    },
    synchronizator: {
        cronPattern: '0 */1 * * *'
    }
};
