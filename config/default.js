'use strict';
module.exports = {
    wp: {
        api: {
            endpoint: 'https://mamydoprace.cz/wp-json',
            username: process.env.WP_USER,
            password: process.env.WP_PASS,
            auth: true
        }
    },
    mongo: {
        uri: 'mongodb://localhost:27017/maminy'
    },
    odata: {
        mountPath: '/',
        port: 3000
    },
    synchronizator: {
        cronPattern: '*/20 * * * *'
    }
};
